package com.complimakers.complimaker.jsons;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ComplementJson {
    private String complement;
    private String complementType;
}
