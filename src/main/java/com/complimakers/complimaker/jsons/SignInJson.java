package com.complimakers.complimaker.jsons;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SignInJson {
    private String email;
    private String password;
}
