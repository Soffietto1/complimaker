package com.complimakers.complimaker.jsons;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class AuthJson {
    private Long id;
    private String token;
}
