package com.complimakers.complimaker.dao;

import com.complimakers.complimaker.model.Complement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.math.BigInteger;
import java.util.List;

public interface ComplementRepository extends JpaRepository<Complement, Long> {

    @Query(value = "SELECT complement_id FROM complements_users_mtom WHERE user_id = ?1", nativeQuery = true)
    List<BigInteger> getAllByUsersId(Long id);
}
