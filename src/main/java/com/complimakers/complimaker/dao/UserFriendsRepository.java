package com.complimakers.complimaker.dao;

import com.complimakers.complimaker.model.UserFriends;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserFriendsRepository extends JpaRepository<UserFriends, Long> {
}
