package com.complimakers.complimaker.dao;

import com.complimakers.complimaker.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findOneByEmail(String email);

    @Query(value = "SELECT second_friend_id FROM user_friends WHERE first_friend_id = ?1", nativeQuery = true)
    List<BigInteger> getAllFriends(Long id);
}
