package com.complimakers.complimaker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ComplimakerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ComplimakerApplication.class, args);
    }
}
