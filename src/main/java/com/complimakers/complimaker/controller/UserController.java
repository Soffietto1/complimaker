package com.complimakers.complimaker.controller;

import com.complimakers.complimaker.helper.Response;
import com.complimakers.complimaker.service.UserService;
import com.complimakers.complimaker.dto.UserDto;
import com.complimakers.complimaker.model.User;
import com.complimakers.complimaker.model.UserFriends;
import com.complimakers.complimaker.service.SecurityService;
import com.complimakers.complimaker.util.UserConverter;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/secure/v1")
public class UserController {

    private UserService userService;
    private SecurityService securityService;

    @Autowired
    public UserController(UserService userService, SecurityService securityService) {
        this.userService = userService;
        this.securityService = securityService;
    }

    @ApiOperation("Get current user")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token",
                    required = true, dataType = "string", paramType = "header")
    })
    @RequestMapping(value = "/users/current", method = RequestMethod.GET)
    public ResponseEntity<Response<UserDto>> getCurrentUser() {
        User user = securityService.getCurrentUser();
        if (user == null) {
            return new ResponseEntity<>(new Response<>("Not authorized"), HttpStatus.BAD_REQUEST);
        }
        UserDto userDto = UserConverter.userToUserDTO(user);
        return new ResponseEntity<>(new Response<>(null, userDto), HttpStatus.OK);
    }

    @ApiOperation("Get user by id")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token",
                    required = true, dataType = "string", paramType = "header")
    })
    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    public ResponseEntity<Response<UserDto>> getUser(@PathVariable("id") Long id) {
        User user = userService.findOneById(id);
        if (user == null) {
            return new ResponseEntity<>(new Response<>("No such user"), HttpStatus.OK);
        }
        UserDto userDto = UserConverter.userToUserDTO(user);
        return new ResponseEntity<>(new Response<>(null, userDto), HttpStatus.OK);
    }

    @ApiOperation("Get all friends of current user")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token",
                    required = true, dataType = "string", paramType = "header")
    })
    @RequestMapping(value = "/users/current/friends", method = RequestMethod.GET)
    public ResponseEntity<Response<List<UserDto>>> getAllCurrentFriends() {
        User user = securityService.getCurrentUser();
        if (user == null) {
            return new ResponseEntity<>(new Response<>("Not authorized"), HttpStatus.BAD_REQUEST);
        }
        List<User> friends = userService.getAllFriends(user);
        if (friends == null) {
            return new ResponseEntity<>(new Response<>("No friends"), HttpStatus.OK);
        }
        List<UserDto> friendsDto = UserConverter.listToDtoList(friends);
        return new ResponseEntity<>(new Response<>(null, friendsDto), HttpStatus.OK);
    }

    @ApiOperation("Get users friends by id")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token",
                    required = true, dataType = "string", paramType = "header")
    })
    @RequestMapping(value = "/users/{id}/friends", method = RequestMethod.GET)
    public ResponseEntity<Response<List<UserDto>>> getAllFriends(@PathVariable("id") Long id) {
        User user = userService.findOneById(id);
        if (user == null) {
            return new ResponseEntity<>(new Response<>("Wrong id"), HttpStatus.BAD_REQUEST);
        }
        List<User> friends = userService.getAllFriends(user);
        if (friends == null) {
            return new ResponseEntity<>(new Response<>("No friends"), HttpStatus.OK);
        }
        List<UserDto> friendsDto = UserConverter.listToDtoList(friends);
        return new ResponseEntity<>(new Response<>(null, friendsDto), HttpStatus.OK);
    }

    @ApiOperation("Add to friends some user from current")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token",
                    required = true, dataType = "string", paramType = "header")
    })
    @RequestMapping(value = "/users/current/friends/{id}", method = RequestMethod.POST)
    public ResponseEntity<Response> addToFriendsCurrent(@PathVariable("id") Long id) {
        User currentUser = securityService.getCurrentUser();
        if(currentUser == null) {
            return new ResponseEntity<>(new Response<>("Not authorized"), HttpStatus.BAD_REQUEST);
        }
        User friend = userService.findOneById(id);
        if(friend == null) {
            return new ResponseEntity<>(new Response<>("No such user"), HttpStatus.BAD_REQUEST);
        }
        UserFriends userFriends = new UserFriends();
        userFriends.setFrindOne(currentUser);
        userFriends.setFriendTwo(friend);
        userService.add(userFriends);
        return new ResponseEntity<>(new Response(), HttpStatus.OK);
    }
}
