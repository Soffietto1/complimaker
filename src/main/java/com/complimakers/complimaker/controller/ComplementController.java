package com.complimakers.complimaker.controller;

import com.complimakers.complimaker.helper.Response;
import com.complimakers.complimaker.service.ComplementService;
import com.complimakers.complimaker.service.UserService;
import com.complimakers.complimaker.dto.ComplementDto;
import com.complimakers.complimaker.jsons.ComplementJson;
import com.complimakers.complimaker.model.Complement;
import com.complimakers.complimaker.model.User;
import com.complimakers.complimaker.service.SecurityService;
import com.complimakers.complimaker.util.ComplementConverter;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/secure/v1")
public class ComplementController {

    private ComplementService complementService;
    private SecurityService securityService;
    private UserService userService;

    @Autowired
    public ComplementController(ComplementService complementService,
                                SecurityService securityService,
                                UserService userService) {
        this.complementService = complementService;
        this.securityService = securityService;
        this.userService = userService;
    }

    @ApiOperation("Get all complements")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token",
                    required = true, dataType = "string", paramType = "header")
    })
    @RequestMapping(value = "/complements", method = RequestMethod.GET)
    public ResponseEntity<Response<List<ComplementDto>>> getAllComplement() {
        List<Complement> complements = complementService.getAll();
        if (complements.isEmpty()) {
            return new ResponseEntity<>(new Response<>("No complements"), HttpStatus.OK);
        }
        List<ComplementDto> complementDtos = ComplementConverter.listToDtoList(complements);
        return new ResponseEntity<>(new Response<>(null, complementDtos), HttpStatus.OK);
    }

    @ApiOperation("Create complement")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token",
                    required = true, dataType = "string", paramType = "header")
    })
    @RequestMapping(value = "/complements", method = RequestMethod.POST)
    public ResponseEntity<Response<ComplementDto>> createComplement(@RequestBody ComplementJson complementJson) {
        Complement complement = ComplementConverter.jsonToComplementConverter(complementJson);
        if (complement.getComplementType() == null) {
            return new ResponseEntity<>(new Response<>("Wrong complement type"), HttpStatus.BAD_REQUEST);
        }
        complementService.add(complement);
        return new ResponseEntity<>(
                new Response<>(null, ComplementConverter.complementToDto(complement)),
                HttpStatus.OK);
    }

    @ApiOperation("Add complement to some user")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token",
                    required = true, dataType = "string", paramType = "header")
    })
    @RequestMapping(value = "/users/current/{user_id}/complements/{complement_id}", method = RequestMethod.POST)
    public ResponseEntity<Response<ComplementDto>> addComplement(@PathVariable(name = "user_id") Long userId,
                                                                 @PathVariable(name = "complement_id") Long complementId) {
        User currentUser = securityService.getCurrentUser();
        if (currentUser == null) {
            return new ResponseEntity<>(new Response<>("Not authorized"), HttpStatus.BAD_REQUEST);
        }
        User someUser = userService.findOneById(userId);
        if (someUser == null) {
            return new ResponseEntity<>(new Response<>("No such user"), HttpStatus.BAD_REQUEST);
        }
        Complement complement = complementService.findOneById(complementId);
        if (complement == null) {
            return new ResponseEntity<>(new Response<>("No such complement"), HttpStatus.BAD_REQUEST);
        }
        List<Complement> allUsersComplements = complementService.getAllByUserId(userId);
        allUsersComplements.add(complement);
        someUser.setComplements(allUsersComplements);
        userService.update(someUser);
        return new ResponseEntity<>(new Response<>(), HttpStatus.OK);
    }

    @ApiOperation("Get all complements by current")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token",
                    required = true, dataType = "string", paramType = "header")
    })
    @RequestMapping(value = "/users/current/complements", method = RequestMethod.GET)
    public ResponseEntity<Response<List<ComplementDto>>> getAllByCurrent() {
        User currentUser = securityService.getCurrentUser();
        if (currentUser == null) {
            return new ResponseEntity<>(new Response<>("Not authorized"), HttpStatus.BAD_REQUEST);
        }
        List<Complement> allUsersComplements = complementService.getAllByUserId(currentUser.getId());
        return new ResponseEntity<>(new Response<>(null, ComplementConverter.listToDtoList(allUsersComplements)), HttpStatus.OK);
    }
}
