package com.complimakers.complimaker.controller;

import com.complimakers.complimaker.helper.Response;
import com.complimakers.complimaker.jsons.SignInJson;
import com.complimakers.complimaker.jsons.SignUpJson;
import com.complimakers.complimaker.service.UserService;
import com.complimakers.complimaker.util.UserConverter;
import com.complimakers.complimaker.jsons.AuthJson;
import com.complimakers.complimaker.model.User;
import com.complimakers.complimaker.service.SecurityService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.ConstraintViolationException;

@RestController
@RequestMapping("/v1")
public class AuthController {

    private UserService userService;
    private SecurityService securityService;
    private static final PasswordEncoder encoder = new BCryptPasswordEncoder();
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public AuthController(UserService userService, SecurityService securityService) {
        this.userService = userService;
        this.securityService = securityService;
    }

    @ApiOperation("Sign in")
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<Response<AuthJson>> loginAndGetToken(@RequestBody SignInJson signInJson) {
        String email = signInJson.getEmail();
        String password = signInJson.getPassword();
        User user = userService.findOneByEmail(email);
        if (user == null) {
            return new ResponseEntity<>(new Response<>("Wrong email"), HttpStatus.BAD_REQUEST);
        }
        if (!encoder.matches(password, user.getPassword())) {
            return new ResponseEntity<>(new Response<>("Wrong password"), HttpStatus.BAD_REQUEST);
        }
        String token = securityService.generateToken(email, password);
        AuthJson authJson = new AuthJson(user.getId(), token);
        return new ResponseEntity<>(new Response<>(null, authJson), HttpStatus.OK);
    }

    @ApiOperation("Sign up")
    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public ResponseEntity<Response<AuthJson>> signUp(@RequestBody SignUpJson signUpJson) {
        User user = userService.findOneByEmail(signUpJson.getEmail());
        if (user != null) {
            return new ResponseEntity<>(new Response<>("Already exists"), HttpStatus.BAD_REQUEST);
        }
        user = UserConverter.jsonToUserConvert(signUpJson);
        try {
            userService.add(user);
        } catch (ConstraintViolationException ex) {
            return new ResponseEntity<>(new Response<>("Wrong input data"), HttpStatus.BAD_REQUEST);
        }
        Long userId = userService.findOneByEmail(signUpJson.getEmail()).getId();
        String token = securityService.generateToken(signUpJson.getEmail(), signUpJson.getPassword());
        AuthJson authJson = new AuthJson(userId, token);
        return new ResponseEntity<>(new Response<>(null, authJson), HttpStatus.OK);
    }
}
