package com.complimakers.complimaker.service;


import com.complimakers.complimaker.model.User;

public interface SecurityService {
    User getCurrentUser();

    String getCurrentUsersEmail();

    String generateToken(String email, String password);
}
