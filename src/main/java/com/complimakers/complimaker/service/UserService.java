package com.complimakers.complimaker.service;

import com.complimakers.complimaker.model.User;
import com.complimakers.complimaker.model.UserFriends;

import java.util.List;

public interface UserService {
    void add(User user);
    void add(UserFriends userFriends);

    User findOneById(Long id);

    User findOneByEmail(String email);

    List<User> getAllFriends(User user);

    boolean update(User user);

    boolean delete(User user);
    boolean delete(UserFriends userFriends);
}
