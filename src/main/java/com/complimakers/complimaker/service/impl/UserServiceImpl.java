package com.complimakers.complimaker.service.impl;

import com.complimakers.complimaker.service.UserService;
import com.complimakers.complimaker.dao.UserFriendsRepository;
import com.complimakers.complimaker.dao.UserRepository;
import com.complimakers.complimaker.model.User;
import com.complimakers.complimaker.model.UserFriends;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserFriendsRepository userFriendsRepository;

    @Override
    public void add(User user) {
        userRepository.save(user);
    }

    @Override
    public void add(UserFriends userFriends) {
        userFriendsRepository.save(userFriends);
    }

    @Override
    public User findOneById(Long id) {
        return userRepository.findOne(id);
    }

    @Override
    public User findOneByEmail(String email) {
        return userRepository.findOneByEmail(email);
    }

    @Override
    public List<User> getAllFriends(User user) {
        List<BigInteger> friendsIds = userRepository.getAllFriends(user.getId());
        return friendsIds
                .stream()
                .map(BigInteger::longValue)
                .map(userRepository::findOne)
                .collect(Collectors.toList());
    }

    @Override
    public boolean update(User user) {
        if (userRepository.exists(user.getId())) {
            userRepository.save(user);
            return true;
        }
        return false;
    }

    @Override
    public boolean delete(User user) {
        if (userRepository.exists(user.getId())) {
            userRepository.delete(user);
            return true;
        }
        return false;
    }

    @Override
    public boolean delete(UserFriends userFriends) {
        if (userFriendsRepository.exists(userFriends.getId())) {
            userFriendsRepository.delete(userFriends);
            return true;
        }
        return false;
    }
}
