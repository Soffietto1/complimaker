package com.complimakers.complimaker.service.impl;

import com.complimakers.complimaker.service.ComplementService;
import com.complimakers.complimaker.dao.ComplementRepository;
import com.complimakers.complimaker.model.Complement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class ComplementServiceImpl implements ComplementService {

    @Autowired
    private ComplementRepository complementRepository;

    @Override
    public void add(Complement complement) {
        complementRepository.save(complement);
    }

    @Override
    public Complement findOneById(Long id) {
        return complementRepository.findOne(id);
    }

    @Override
    public List<Complement> getAll() {
        return complementRepository.findAll();
    }

    @Override
    public List<Complement> getAllByUserId(Long id) {
        List< BigInteger> ids = complementRepository.getAllByUsersId(id);
        return ids
                .stream()
                .map(BigInteger::longValue)
                .map(complementRepository::findOne)
                .collect(Collectors.toList());
    }
}
