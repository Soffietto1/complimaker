package com.complimakers.complimaker.service;

import com.complimakers.complimaker.model.Complement;

import java.util.List;

public interface ComplementService {
    void add(Complement complement);

    Complement findOneById(Long id);

    List<Complement> getAll();

    List<Complement> getAllByUserId(Long id);
}
