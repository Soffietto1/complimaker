package com.complimakers.complimaker.helper;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Response<B> {
    private String message;
    private B body;

    public Response(String message) {
        this.message = message;
    }
}
