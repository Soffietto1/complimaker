package com.complimakers.complimaker.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "users")
public class User extends AbstractEntity implements Serializable {

    @Email
    private String email;
    @NotBlank
    private String password;
    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;
    private String middleName;
    private LocalDate dateOfBirth;
    private Long rating;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name="complements_users_mtom", joinColumns = {
            @JoinColumn(name = "user_id",nullable = true, updatable = false) },
            inverseJoinColumns = {@JoinColumn(name = "complement_id",
                    updatable = false)})
    private List<Complement> complements;
}
