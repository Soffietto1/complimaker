package com.complimakers.complimaker.model.enums;

public enum ComplementType {
    SIMPLE_COMPLEMENT, ADVANCED_COMPLEMENT
}
