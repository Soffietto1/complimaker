package com.complimakers.complimaker.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "user_friends")
public class UserFriends extends AbstractEntity {

    @ManyToOne
    @JoinColumn(name = "first_friend_id")
    private User frindOne;
    @ManyToOne
    @JoinColumn(name = "second_friend_id")
    private User friendTwo;
}
