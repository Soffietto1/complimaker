package com.complimakers.complimaker.model;

import com.complimakers.complimaker.model.enums.ComplementType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "complements")
public class Complement extends AbstractEntity {

    private String complement;

    @Enumerated(EnumType.STRING)
    private ComplementType complementType;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "complements")
    private List<User> users;
}
