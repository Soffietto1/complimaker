package com.complimakers.complimaker.util;

import com.complimakers.complimaker.dto.UserDto;
import com.complimakers.complimaker.jsons.SignUpJson;
import com.complimakers.complimaker.model.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;
import java.util.stream.Collectors;

public class UserConverter {

    private static final PasswordEncoder encoder = new BCryptPasswordEncoder();

    public static User jsonToUserConvert(SignUpJson signUpJson) {
        User user = new User();
        user.setEmail(signUpJson.getEmail());
        user.setPassword(encoder.encode(signUpJson.getPassword()));
        user.setFirstName(signUpJson.getFirstName());
        String middleName = signUpJson.getMiddleName();
        if(middleName != null) {
            user.setMiddleName(middleName);
        }
        user.setLastName(signUpJson.getLastName());
        user.setDateOfBirth(signUpJson.getDateOfBirth());
        return user;
    }

    public static UserDto userToUserDTO(User user) {
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setEmail(user.getEmail());
        userDto.setFirstName(user.getFirstName());
        if (user.getMiddleName() != null) userDto.setMiddleName(user.getMiddleName());
        userDto.setLastName(user.getLastName());
        userDto.setDateOfBirth(user.getDateOfBirth());
        userDto.setRating(user.getRating());
        return userDto;
    }

    public static List<UserDto> listToDtoList(List<User> users) {
        return users.stream()
                .map(UserConverter::userToUserDTO)
                .collect(Collectors.toList());
    }
}
