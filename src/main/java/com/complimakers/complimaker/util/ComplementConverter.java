package com.complimakers.complimaker.util;

import com.complimakers.complimaker.dto.ComplementDto;
import com.complimakers.complimaker.jsons.ComplementJson;
import com.complimakers.complimaker.model.Complement;
import com.complimakers.complimaker.model.enums.ComplementType;

import java.util.List;
import java.util.stream.Collectors;

public class ComplementConverter {

    public static List<ComplementDto> listToDtoList(List<Complement> complements) {
        return complements.stream()
                .map(ComplementConverter::complementToDto)
                .collect(Collectors.toList());
    }

    public static ComplementDto complementToDto(Complement complement) {
        ComplementDto complementDto = new ComplementDto();
        complementDto.setId(complement.getId());
        complementDto.setComplement(complement.getComplement());
        complementDto.setComplementType(complement.getComplementType());
        return complementDto;
    }

    public static Complement jsonToComplementConverter(ComplementJson complementJson) {
        Complement complement = new Complement();
        complement.setComplement(complementJson.getComplement());
        complement.setComplementType(ComplementType.valueOf(complementJson.getComplementType()));
        return complement;
    }
}
