package com.complimakers.complimaker.dto;

import com.complimakers.complimaker.model.enums.ComplementType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ComplementDto {

    private Long id;
    private String complement;
    private ComplementType complementType;
}
